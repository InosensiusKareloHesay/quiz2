<?php
function hitung($string_data){
    if (preg_match("/\*/", $string_data)){
        $pecah = explode("*", $string_data);
        return $pecah[0]*$pecah[1];
        }else if(preg_match("/:/", $string_data)){
        $pecah = explode(":", $string_data);
        return $pecah[0]/$pecah[1];
        }else if(preg_match("/%/", $string_data)){
        $pecah = explode("%", $string_data);
        return $pecah[0]%$pecah[1];
        }else if(preg_match("/\+/", $string_data)){
        $pecah = explode("+", $string_data);
        return $pecah[0]+$pecah[1];
        }else if(preg_match("/-/", $string_data)){
        $pecah = explode("-", $string_data);
        return $pecah[0]-$pecah[1];
    }   
}

echo hitung("102*2");
echo hitung("2+3");
echo hitung("100:25");
echo hitung("10%2");
echo hitung("99-2");

?>
